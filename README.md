**Set-up**

1. Clone this project from GitLab:

    ```bash
    git clone \
        --branch v2 --single-branch --depth 1 --no-tags \
        https://gitlab.com/manumuttathu1996/bda-htw-berlin-ss20
    ```

2. Create `$HOME/data/postgres` directory for PostgreSQL files: `mkdir -p ~/data/postgres`
3. Optional, for local development, install Python packages: `python3 -m pip install -r requirements.txt`
4. Optional, pull docker images first:

    ```bash
	    
    docker pull manumuttathu/assignment:v1
    	docker pull manumuttathu/jyupter-all-spark
    	docker pull manumuttathu/postgres
    	docker pull manumuttathu/pgadmin4 
    ```

5. From terminal run docker-compose up
6. Retrieve the token to log into Jupyter: `docker logs $(docker ps | grep jupyter_spark | awk '{print $NF}')`
7. Run notebook (implement on postgres admin server the csv-file which was uploaded on frachtwerk repository through Tools -> Import/Export)

**Demo**
1. Open from jupytr console BDA_AssignmentII02.02.21 

